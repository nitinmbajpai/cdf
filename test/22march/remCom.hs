import System.IO

remLine [] = []
remLine ('\n':st) = ('\n':st)
remLine (x:st) = remLine st

reCom1 [x] = [x]
remCom1 ('-':'}':st) = st
remCom1 (x:st) = remCom1 st

remCom [] f = []
remCom [x] f = [x]
remCom ('"':st) f = '"':remCom st (1-f)
remCom ('-':'-':st) 0 = remCom (remLine st) 0
remCom ('{':'-':st) 0 = remCom (remCom1 st) 0
remCom (x:st) f = x:remCom st f

main = do
  str <- readFile "input.hs"
  writeFile "output.hs" (remCom str 0)
 