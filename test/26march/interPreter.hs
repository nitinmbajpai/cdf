data Ae = Add Ae Ae | Sub Ae Ae | Mult Ae Ae | Div Ae Ae | Value Int | Var Char deriving (Show, Eq)
data Be = L Ae Ae | G Ae Ae | E Ae Ae | LE Ae Ae | GE Ae Ae | NE Ae Ae | And Be Be | Or Be Be | Not Be deriving (Show, Eq)                           
data Lang = Assign Char Ae | While Be [Lang] deriving (Show, Eq)

prog = [Assign 'B' (Value 10), Assign 'A' (Value 1), While (L (Var 'A') (Var 'B')) [Assign 'A' (Add (Var 'A') (Value 1))]]

intPtr :: [Lang] -> [(Char,Int)]
-- intPtr prog

iptr [] env = env
iptr ((Assign c ae):xs) env = iptr xs $ (filter (\(x,y) -> x/=c) env) ++ [(c,evalAe ae  env)]
iptr ((While be ls):xs) env = iptr xs $ while be ls env

while be ls env | evalBe be env = while be ls $ iptr ls env
                | otherwise = env
                              
evalAe (Add a b) env = evalAe a env + evalAe b env
evalAe (Sub a b) env = evalAe a env - evalAe b env 
evalAe (Mult a b) env = evalAe a env * evalAe b env
evalAe (Value a) env = a
evalAe (Var a) env = snd $ head $ filter (\(x,y) -> x==a) env  
  
evalBe (L a b) env = evalAe a env < evalAe b env                              
evalBe (G a b) env = evalAe a env > evalAe b env                              
evalBe (E a b) env = evalAe a env == evalAe b env                              
evalBe (LE a b) env = evalAe a env <= evalAe b env                              
evalBe (GE a b) env = evalAe a env >= evalAe b env                              
evalBe (NE a b) env = evalAe a env /= evalAe b env                              
evalBe (And a b) env = evalBe a env && evalBe b env                              
evalBe (Or a b) env = evalBe a env || evalBe b env                              
evalBe (Not a) env = not $ evalBe a env                              

intPtr l = iptr l []