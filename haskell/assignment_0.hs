
--0. [1,2,3]->123
l2i :: [Int]->Int
l2i l = foldr (\ a b -> b*10+a) 0 (reverse l)

--or

l2i1 :: [Int]->Int
l2i1 l = foldl (\ a b -> a*10+b) 0 l
-----------------------------------------------------------------------
--1. myunzip

myunzip :: [(a,b)]->([a],[b])
myunzip l = foldr (\ (a,b) (l1,l2) -> (a:l1,b:l2)) ([],[]) l

--or

myunzip1 :: [(a,b)]->([a],[b])
myunzip1 l = foldl (\ (l1,l2) (a,b) -> (l1++[a],l2++[b])) ([],[]) l
------------------------------------------------------------------------
--2 myzip
{-
myzip :: [a]->[b]->[(a,b)]
myzip l1 l2 = foldr (\ x ([],c) ->) (foldr (\ a b -> (a,length l -1):b) [] l1, 0) l2
-}
------------------------------------------------------------------------
--3 mylen 

mylen :: [a]->Int
mylen l = foldr (\ x y -> y+1) 0 l

--or

mylen1 :: [a]->Int
mylen1 l = foldl (\ x y -> x+1) 0 l
------------------------------------------------------------------------
--4 myelem

myelem :: Eq a => a -> [a] -> Bool
myelem x l = foldr (||) False (map (==x) l)

--or

myelem1 :: Eq a => a -> [a] -> Bool
myelem1 x l = foldl (||) False (map (==x) l)
------------------------------------------------------------------------
--5 felem (find index of element)

felem :: Eq a => a -> [a] -> Maybe Int
felem x l = fst(foldr (\ a (b,c) -> if a==x then (Just c,c-1) else (b,c-1)) (Nothing,length l -1) l)

--or

felem1 :: Eq a => a -> [a] -> Maybe Int
felem1 x l = fst(foldl (\ (b,c) a -> if a==x then (Just c,c-1) else (b,c-1)) (Nothing,0) l)
------------------------------------------------------------------------
--6 myfilter
myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f l = concatMap (\ a -> if (f a) then [a] else []) l

--or

myfilter1 f l = foldr (\ a b -> if (f a) then a:b else b) [] l

--or

myfilter2 f l = foldl (\ b a -> if (f a) then b++[a] else b) [] l
------------------------------------------------------------------------
--7 flatten

flatten :: [[a]] -> [a]
flatten l = foldr (++) [] l

--or

flatten1 :: [[a]] -> [a]
flatten1 l = foldl (++) [] l
------------------------------------------------------------------------
--8 insert (insert in sorted list)

insert :: Int -> [Int] -> [Int]
insert x [] = [x]
insert x l | head l > x = x:l
       	   | otherwise = foldr (\ a b -> if ((a <= x) && (b==[] || head b > x)) then a:x:b else a:b) [] l

--or

insert1 x [] = [x]
insert1 x l | last l < x = l++[x]
       	   | otherwise = foldl (\ b a -> if ((a >= x) && (b==[] || last b < x)) then b++[x]++[a] else b++[a]) [] l
------------------------------------------------------------------------
--9 sort

sort :: [Int] -> [Int]
sort l = foldr insert [] l 

--or

sort1 :: [Int] -> [Int]
sort1 l = foldl (flip insert) [] l 
------------------------------------------------------------------------
--10 delete (delete Node from bst)

data Bst a = Empty | Node a (Bst a) (Bst a) deriving (Show, Eq)

bst1 = Node 5 (Node 2 (Node 1 Empty Empty) (Node 3 Empty (Node 4 Empty Empty))) (Node 8 (Node 7 (Node 6 Empty Empty) Empty) (Node 9 Empty Empty))

delete :: Eq a => a -> Bst a -> Bst a
delete x Empty = Empty
delete x (Node a l r) | a /= x = Node a (delete x l) (delete x r)
       	       	      | otherwise = if r == Empty then l else (rct l r)

 where 
      rct lt Empty = lt
      rct lt (Node a l r) = Node a (rct lt l) r