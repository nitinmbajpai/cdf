duplicates [] = []
duplicates (x:xs) = x : duplicates (filter (/=x) xs)

adjDuplicates [] = []
adjDuplicates (x:xs) = x : adjDuplicates (dropWhile (x==) xs)

