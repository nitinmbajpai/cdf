1)
sumTC2 (Leaf x) k = k x
sumTC2 (Node a left right) k =
       sumTC2 left (\sleft -> sumTC2 right (\sright -> k (a + sleft + sright)))

sumTC2 (Node 1 (Node 2 (Leaf 3) (Leaf 4)) (Leaf 5)) id
= sumTC2 (Node 2 (Leaf 3) (Leaf 4)) (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright)))
= sumTC2 (Leaf 3) (\sleft1 -> sumTC2 (Leaf 4) (\sright1 -> (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) (2 + sleft1 + sright1)))
= (\sleft1 -> sumTC2 (Leaf 4) (\sright1 -> (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) (2 + sleft1 + sright1))) 3
= sumTC2 (Leaf 4) (\sright1 -> (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) (2 + 3 + sright1))
= (\sright1 -> (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) (2 + 3 + sright1)) 4
= (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) (2 + 3 + 4)
= sumTC2 (Leaf 5) (\sright -> id (1 + (2 + 3 + 4) + sright))
= (\sright -> id (1 + (2 + 3 + 4) + sright)) 5
= id (1 + (2 + 3 + 4) + 5)
= 15

2)
factC 0 k = k 1
factC n k = factC (n - 1) $ \m -> k ( n * m)

factC 4 id
= factC 3 $ \m -> id (4 * m)
= factC 2 $ \m1 -> (\m -> id (4 * m)) (3 * m1)
= factC 1 $ \m2 -> (m1 -> (\m -> id (4 * m)) (3 * m1)) (2 * m2)
= factC 0 $ \m3 -> (m2 -> (m1 -> (\m -> id (4 * m)) (3 * m1)) (2 * m2)) (1 * m3)
= (\m3 -> (m2 -> (m1 -> (\m -> id (4 * m)) (3 * m1)) (2 * m2)) (1 * m3)) 1
= (m2 -> (m1 -> (\m -> id (4 * m)) (3 * m1)) (2 * m2)) (1 * 1)
= (m1 -> (\m -> id (4 * m)) (3 * m1)) (2 * (1 * 1))
= (\m -> id (4 * m)) (3 * (2 * (1 * 1)))
= id (4 * (3 * (2 * (1 * 1))))
= 24