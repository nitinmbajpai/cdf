import qualified Data.Map as Map
import Data.Maybe 
import Control.Monad.State
import Control.Monad.Writer

type Env   = Map.Map Char Int
type Error = String

data AExp = Var Char
            | Value Int
            | Plus AExp AExp
            | Mod AExp AExp
            | Mul AExp AExp

currEnv = Map.fromList[('X',2),('Y',3),('Z',0)]

--runState (aEval (Plus (Var 'X') (Var 'Y'))) (0,[])

aEval :: AExp -> State (Int,[String]) Int
aEval (Value v)        =  get >>= \(x,y) -> put (x+1,y++["Evaluated a value"]) >> return v
aEval (Var k)          =  get >>= 
                               \(x,y) -> put (x+1,y++["Evaluated a Variable"]) >>
                                               (return . fromJust . Map.lookup k $ currEnv)
aEval (Plus ax1 ax2)   =  do
                             (x,y) <- get
                             put (x+1, y++["Evaluated a Plus"])
                             --modify (+1) 
                             val1 <- aEval ax1
                             val2 <- aEval ax2
                             return (val1 + val2) 

------------------------------------------------------------------------------------------------

--runState (aEval1 (Plus (Var 'X') (Var 'Y'))) 0

aEval1 :: AExp -> State Int (Either String Int)
aEval1 (Value v)        =  get >>= \x -> put (x+1) >> return (Right v)
aEval1 (Var k)          =  get >>= \x -> put (x+1) >> 
                                         case (Map.lookup k currEnv) of
                                           Nothing -> return (Left $ "unbound variable " ++ [k])
                                           Just a  -> return (Right a) 
aEval1 (Plus ax1 ax2)   =  do
                             modify (+1)                                                    
                             val1 <- aEval1 ax1
                             val2 <- aEval1 ax2                             
                             
                             case (val1,val2) of 
                               (Left e,_) -> return (Left e)
                               (_,Left e) -> return (Left e)
                               (Right v1,Right v2) -> return $ Right (v1 + v2)